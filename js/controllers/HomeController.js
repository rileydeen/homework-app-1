//This is the controller class that acts for the application when things such as classes and color are added.

app.controller('HomeController', ['$scope', '$location', '$route', '$firebaseAuth', '$firebaseObject', function($scope, $location, $route, $firebaseAuth, $firebaseObject) {

    //var chat_div = document.getElementById("tasks");
    $scope.task = {};
    $scope.class_to_add = {}; //Holds class to add
    $scope.classes = []; //Holds list of classes
    $scope.auth = $firebaseAuth();
    $scope.uid = $scope.auth.$getAuth().uid;
    $scope.user_data = {};
    $scope.user_data.tasks = [];

    // firebase object testing
    var ref = firebase.database().ref('users/' + $scope.uid);
    //ref.set({tasks: ["hi"]});
    var obj = $firebaseObject(ref);
    obj.$bindTo($scope, 'user_data').then(function(){
        console.log($scope.user_data);
        if (!$scope.user_data.tasks){
            $scope.user_data.tasks = ["PLACEHOLDER"];  // need to put something to create array
        }
        //$scope.tasks = ["hi"];
        console.log($scope.user_data.tasks);
    })
    console.log($scope.uid);


    /////////////// add tasks /////////////////
    $scope.addTask = function() {
        console.log("in addTask()");

        if (!$scope.task.task || !$scope.task.date)
            return;

        // testing saving tasks
        if($scope.user_data.tasks[0] === "PLACEHOLDER"){
            $scope.user_data.tasks[0] = $scope.task;
        }
        else {
            $scope.user_data.tasks.push($scope.task);
        }
        //$scope.user_data.tasks.push($scope.task);

        //////////// add task to div //////////////
        var task_html;
        var divElement = angular.element(document.querySelector('#tasks'));

        // build task html
        task_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-left'>\
                     <div class='list-group'><div class='list-group-item' \
                     style='background-color: " + $scope.task.class.color + "'><h4 class='list-group-item-heading'>\
                     " + $scope.task.class.name + "</h4><h4 class='list-group-item-heading'>\
                     Duedate: " + $scope.task.date + "</h4><p class='list-group-item-text'>\
                     " + $scope.task.task + "</p></div></div></div></div>";

        // Add the new task
        divElement.append(task_html);

        // Scroll div to bottom
        task_div = document.getElementById("tasks");
        task_div.scrollTop = task_div.scrollHeight;

        // Clear out message holder
        $scope.task = {};
    };

    $scope.addClass = function() {
        $scope.classes[$scope.classes.length] = $scope.class_to_add;
        $scope.class_to_add = {};
    };

    $scope.pickClass = function(task_class) {
        console.log(task_class);
        $scope.task.class = task_class;
    };

    $scope.logout = function(){
        $scope.auth.$signOut().then(function() {
            $location.path('/login');
        });
    };

}]);