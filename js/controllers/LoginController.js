app.controller('LoginController', ['$scope', '$location', '$route', '$firebaseAuth', function($scope, $location, $route, $firebaseAuth) {

    // make sure not logged in already
    var auth = $firebaseAuth();
    if (auth.$getAuth()){
        $location.path('/home');
    }

    // create user function
    $scope.create_user = function() {
        auth.$createUserWithEmailAndPassword($scope.account.email, $scope.account.password).then(function(firebaseUser) {
            $scope.firebaseUser = firebaseUser;
            $location.path('/home');
            $route.reload();
        }).catch(function(error) {
            $scope.error = error;
            console.log(error);
        });
    };

    // login function
    $scope.login = function() {
        auth.$signInWithEmailAndPassword($scope.account.email, $scope.account.password).then(function(firebaseUser) {
            $scope.firebaseUser = firebaseUser;
            $location.path('/home');
            $route.reload();
        }).catch(function(error) {
            $scope.error = error;
        });
    };

}]);